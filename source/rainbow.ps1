. "$HOME/.stdmatt/lib/shlib/shlib.ps1"

function rbow2()
{
    $text   = "";
    $colors = @{};

    for($i = 0; $i -lt $args.Count; $i += 1) {
        $arg = $args[$i];
        if($arg.StartsWith("-")) {
            $clean_name = $arg.Substring(1);
            $colors[$clean_name] = $args[$i + 1];
            $i = ($i + 1);
        } else {
            $text += $args[$i];
        }
    }

    foreach($key in $colors.Keys) {
        $ansi_escape = (sh_make_ansi_hex_color $colors[$key]);
        $text = ($text -replace "<${key}>", $ansi_escape)
    }

    return $text;
}

function rbow_colored()
{
    Param(
        $text,
        $fg,
        $bg
    );

    $color = (sh_make_ansi_hex_color $fg $bg);
    $reset = (sh_make_ansi_color 0);

    return "${color}${text}${reset}";
}


##
## RBOW
##

function _hex_from_arg()
{
    $v = $args[0];

    if(-not $v)       { return ""; }
    if($v[0] -eq "#") { return $v; }

    ## Colors
    elseif($v -eq "transparent") { return "" }

    elseif($v -eq "path1" ) { return "#F0C674" }
    elseif($v -eq "path2" ) { return "#C5C8C6" }
    elseif($v -eq "action") { return "#F0C674" }
    elseif($v -eq "done"  ) { return "#00FF00" }

    return "";
}

##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
$SCRIPT_FULLPATH = $MyInvocation.MyCommand.Path;
$SCRIPT_DIR      = (Split-Path "$SCRIPT_FULLPATH" -Parent);
$HOME_DIR        = if($HOME -eq "") { "$env:USERPROFILE" } else { $HOME };
$LIB_NAME        = "rainbow";
$INSTALL_DIR     = (Join-Path $HOME_DIR ".stdmatt/lib/$LIB_NAME");

##----------------------------------------------------------------------------##
## Install                                                                    ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
Write-Output "Installing [$LIB_NAME]";

New-Item -Path "${INSTALL_DIR}" -ItemType Directory -Force;

## @XXX(before-1.0.0): Linking instead of copying just to be able to
## edit in the early dev iterations... - stdmatt 22-03-21 at 19:59
ln -f "${SCRIPT_DIR}/source/${LIB_NAME}.ps1" "${INSTALL_DIR}/${LIB_NAME}.ps1";

Write-Output "Done..."
